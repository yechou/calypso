#!/usr/bin/env python
"""
Produce particle gun samples
Derived from G4FaserAlgConfigNew
This is a general low-level script, 
although this could be useful for testing.

Usage:
faser_particlegun.py <options>

Control using command-line options:
--evtMax=1000
--skipEvt=1000

Output.HITSFileName=<name>
Sim.Gun='{"pid" : 11, "z": -1500.}'

Copyright (C) 2002-2021 CERN for the benefit of the ATLAS and FASER collaborations
"""

if __name__ == '__main__':

    import time
    a = time.time()
#
# Set up logging and config behaviour
#
    from AthenaCommon.Logging import log
    from AthenaCommon.Constants import DEBUG, VERBOSE
    from AthenaCommon.Configurable import Configurable
    log.setLevel(DEBUG)
    Configurable.configurableRun3Behavior = 1
#
# Import and set config flags
#
    from CalypsoConfiguration.AllConfigFlags import ConfigFlags
    ConfigFlags.Exec.MaxEvents = 10  # can be overridden from command line with --evtMax=<number>
    ConfigFlags.Exec.SkipEvents = 0  # can be overridden from command line with --skipEvt=<number>
    from AthenaConfiguration.Enums import ProductionStep
    ConfigFlags.Common.ProductionStep = ProductionStep.Simulation
#
# All these must be specified to avoid auto-configuration
#
    ConfigFlags.Input.RunNumber = [12345] #Isn't updating - todo: investigate
    ConfigFlags.Input.OverrideRunNumber = True
    ConfigFlags.Input.LumiBlockNumber = [1]
    ConfigFlags.Input.isMC = True
#
# Output file name
# 
    ConfigFlags.Output.HITSFileName = "my.HITS.pool.root" # can be overridden from command line with Output.HITSFileName=<name>
#
# Sim ConfigFlags
#
    ConfigFlags.Sim.Layout = "FASER"
    ConfigFlags.Sim.PhysicsList = "FTFP_BERT"
    ConfigFlags.Sim.ReleaseGeoModel = False
    ConfigFlags.Sim.IncludeParentsInG4Event = True # Controls whether BeamTruthEvent is written to output HITS file
    ConfigFlags.addFlag("Sim.Gun",{"Generator" : "SingleParticle"})  # Property bag for particle gun keyword:argument pairs
    ConfigFlags.addFlag("Sim.Beam.xangle", 0)  # Potential beam crossing angles
    ConfigFlags.addFlag("Sim.Beam.yangle", 0)    

    ConfigFlags.GeoModel.FaserVersion = "FASERNU-02"             # Geometry set-up
    ConfigFlags.IOVDb.GlobalTag = "OFLCOND-FASER-02"             # Conditions set-up
    ConfigFlags.addFlag("Input.InitialTimeStamp", 0)             # To avoid autoconfig 
    ConfigFlags.GeoModel.Align.Dynamic = False

#
# Preset particle gun parameters
#
    from math import atan
    from AthenaCommon.SystemOfUnits import GeV, TeV, cm, m
    from AthenaCommon.PhysicalConstants import pi

    # 11 - electron, 13 - muon, 22 - photon
    import ParticleGun as PG
    ConfigFlags.Sim.Gun = {
        "Generator" : "SingleParticle", "pid" : 13, 
        "energy" : PG.LogSampler(10*GeV, 1*TeV), 
        "theta" :  PG.GaussianSampler(0, atan((10*cm)/(7*m)), oneside = True), 
        "phi" : [0, 2*pi], "mass" : 0.511, "radius" : -10*cm, #"z": -2.0*m,
        "randomSeed" : 12345}

#
# Command-line overrides
#
# These have the format: Sim.Gun='{"pid" : 11}'
# Filename: Output.HITSFileName="test.muon.001.root"
# Also number of events: --evtMax 100
# Starting at z = -1.5m (-1500.) will miss the veto (for electrons)

    import sys
    ConfigFlags.fillFromArgs(sys.argv[1:])

#
# By being a little clever, we can steer the geometry setup from the command line using GeoModel.FaserVersion
#
# MDC configuration
#
    detectors = ['Veto', 'Preshower', 'FaserSCT', 'Ecal', 'Trigger', 'Dipole', 'Emulsion']
#
# Setup detector flags
#
    from CalypsoConfiguration.DetectorConfigFlags import setupDetectorsFromList
    setupDetectorsFromList(ConfigFlags, detectors, toggle_geometry=True)
#
# Finalize flags
#
    ConfigFlags.lock()
#
# Initialize a new component accumulator
#
    from CalypsoConfiguration.MainServicesConfig import MainServicesCfg
    cfg = MainServicesCfg(ConfigFlags)
#
# Configure the particle gun as requested, or using defaults
#

#
# Particle gun generators - the generator, energy, angle, particle type, position, etc can be modified by passing keyword arguments
#
    from FaserParticleGun.FaserParticleGunConfig import FaserParticleGunCfg
    cfg.merge(FaserParticleGunCfg(ConfigFlags))
    from McEventSelector.McEventSelectorConfig import McEventSelectorCfg
    cfg.merge(McEventSelectorCfg(ConfigFlags))

#
# Output file
#
    from AthenaPoolCnvSvc.PoolWriteConfig import PoolWriteCfg
    cfg.merge(PoolWriteCfg(ConfigFlags))

#
# Shift LOS
#

    if ConfigFlags.Sim.Beam.xangle or ConfigFlags.Sim.Beam.yangle:
        MCEventKey = "BeamTruthEventShifted"
        import McParticleEvent.Pythonizations
        from GeneratorUtils.ShiftLOSConfig import ShiftLOSCfg
        cfg.merge(ShiftLOSCfg(ConfigFlags, OutputMCEventKey = MCEventKey,
                              xcross = ConfigFlags.Sim.Beam.xangle, ycross = ConfigFlags.Sim.Beam.yangle))
    else:    
        MCEventKey = "BeamTruthEvent"
    
#
# Add the G4FaserAlg
#
    from G4FaserAlg.G4FaserAlgConfigNew import G4FaserAlgCfg
    cfg.merge(G4FaserAlgCfg(ConfigFlags, InputTruthCollection = MCEventKey))
#
# Dump config
#
    from AthenaConfiguration.ComponentFactory import CompFactory
    cfg.addEventAlgo(CompFactory.JobOptsDumperAlg(FileName="G4FaserTestConfig.txt"))
    cfg.getService("StoreGateSvc").Dump = True
    cfg.getService("ConditionStore").Dump = True
    cfg.printConfig(withDetails=True, summariseProps = False)  # gags on ParticleGun if summariseProps = True?

    ConfigFlags.dump()
    #f = open("test.pkl","wb")
    #cfg.store(f)
    #f.close()
#
# Execute and finish
#

    #cfg.foreach_component("*").OutputLevel = "INFO"  # Use warning for production

    sc = cfg.run()

    b = time.time()
    log.info("Run G4FaserAlg in " + str(b-a) + " seconds")
#
# Success should be 0
#
    sys.exit(not sc.isSuccess())

