""" Define methods to configure WaveformRangeTool

Copyright (C) 2022 CERN for the benefit of the FASER collaboration
"""
from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from AthenaConfiguration.ComponentFactory import CompFactory
from IOVDbSvc.IOVDbSvcConfig import addFolders
WaveformRangeTool=CompFactory.WaveformRangeTool

def WaveformRangeToolCfg(flags, name="WaveformRangeTool", **kwargs):
    """ Return a configured WaveformRangeTool"""
    return WaveformRangeTool(name, **kwargs)

def WaveformRangeCfg(flags, **kwargs):
    """ Return configured ComponentAccumulator and tool for Waveform Range 

    WaveformRangeTool may be provided in kwargs
    """

    acc = ComponentAccumulator()
    # tool = kwargs.get("WaveformRangeTool", WaveformRangeTool(flags))
    # Probably need to figure this out!
    dbInstance = kwargs.get("dbInstance", "TDAQ_OFL")
    dbFolder = kwargs.get("dbFolder", "/WAVE/DAQ/Range")
    acc.merge(addFolders(flags, dbFolder, dbInstance, className="CondAttrListCollection"))
    return acc

