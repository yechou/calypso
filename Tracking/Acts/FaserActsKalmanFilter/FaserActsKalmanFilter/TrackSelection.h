#ifndef FASERACTSKALMANFILTER_TRACKSELECTION_H
#define FASERACTSKALMANFILTER_TRACKSELECTION_H

#include "FaserActsKalmanFilter/FaserActsRecMultiTrajectory.h"
#include "TrackerSimData/TrackerSimDataCollection.h"
#include "Acts/TrackFinding/CombinatorialKalmanFilter.hpp"

using TrackFitterResult =
Acts::Result<Acts::CombinatorialKalmanFilterResult<IndexSourceLink>>;
using TrackFinderResult = std::vector<TrackFitterResult>;

struct TrackQuality {
  Acts::CombinatorialKalmanFilterResult<IndexSourceLink> track;
  size_t nMeasurements;
  double chi2;
};

void selectTracks(TrackFinderResult& results, std::vector<TrackQuality>& trackQuality);

#endif  // FASERACTSKALMANFILTER_TRACKSELECTION_H
