#include "FaserActsKalmanFilter/KalmanFitterTool.h"

#include "FaserActsGeometry/FASERMagneticFieldWrapper.h"

#include "Acts/Propagator/EigenStepper.hpp"
#include "Acts/Propagator/Navigator.hpp"
#include "Acts/Propagator/Propagator.hpp"
#include "Acts/TrackFitting/GainMatrixSmoother.hpp"
#include "Acts/TrackFitting/GainMatrixUpdater.hpp"
#include "TrackerIdentifier/FaserSCT_ID.h"

#include "TrackerRIO_OnTrack/FaserSCT_ClusterOnTrack.h"


KalmanFitterTool::KalmanFitterTool(const std::string& type, const std::string& name, const IInterface* parent) :
    AthAlgTool(type, name, parent) {}


StatusCode KalmanFitterTool::initialize() {
  ATH_CHECK(m_fieldCondObjInputKey.initialize());
  ATH_CHECK(m_trackingGeometryTool.retrieve());
  ATH_CHECK(detStore()->retrieve(m_idHelper,"FaserSCT_ID"));
  if (m_statesWriter && !m_noDiagnostics) ATH_CHECK(m_trajectoryStatesWriterTool.retrieve());
  if (m_summaryWriter && !m_noDiagnostics) ATH_CHECK(m_trajectorySummaryWriterTool.retrieve());
  m_fit = makeTrackFitterFunction(m_trackingGeometryTool->trackingGeometry());
  if (m_actsLogging == "VERBOSE" && !m_noDiagnostics) {
    m_logger = Acts::getDefaultLogger("KalmanFitter", Acts::Logging::VERBOSE);
  } else if (m_actsLogging == "DEBUG" && !m_noDiagnostics) {
    m_logger = Acts::getDefaultLogger("KalmanFitter", Acts::Logging::DEBUG);
  } else {
    m_logger = Acts::getDefaultLogger("KalmanFitter", Acts::Logging::INFO);
  }
  return StatusCode::SUCCESS;
}


StatusCode KalmanFitterTool::finalize() {
  return StatusCode::SUCCESS;
}

std::unique_ptr<Trk::Track>
KalmanFitterTool::fit(const EventContext &ctx, const Trk::Track &inputTrack,
                      std::vector<FaserActsRecMultiTrajectory> & /*trajectories*/,
                      const Acts::BoundVector& inputVector = Acts::BoundVector::Zero(), bool isMC=false, double origin=0) const {
  std::unique_ptr<Trk::Track> newTrack = nullptr;
  std::vector<FaserActsRecMultiTrajectory> myTrajectories;

  if (!inputTrack.measurementsOnTrack() || inputTrack.measurementsOnTrack()->size() < m_minMeasurements) {
    ATH_MSG_DEBUG("Input track has no or too little measurements and cannot be fitted");
    return nullptr;
  }

  if (!inputTrack.trackParameters() || inputTrack.trackParameters()->empty()) {
    ATH_MSG_DEBUG("Input track has no track parameters and cannot be fitted");
    return nullptr;
  }


  auto pSurface = Acts::Surface::makeShared<Acts::PlaneSurface>(
      Acts::Vector3 {0, 0, origin}, Acts::Vector3{0, 0, -1});

  Acts::GeometryContext gctx = m_trackingGeometryTool->getNominalGeometryContext().context();
  Acts::MagneticFieldContext mfContext = getMagneticFieldContext(ctx);
  Acts::CalibrationContext calibContext = Acts::CalibrationContext();

  auto [sourceLinks, measurements] = getMeasurementsFromTrack(inputTrack);
  auto trackParameters = getParametersFromTrack(inputTrack.trackParameters()->front(), inputVector, origin);
  ATH_MSG_DEBUG("trackParameters: " << trackParameters.parameters().transpose());
  ATH_MSG_DEBUG("position: " << trackParameters.position(gctx).transpose());
  ATH_MSG_DEBUG("momentum: " << trackParameters.momentum().transpose());
  ATH_MSG_DEBUG("charge: " << trackParameters.charge());

  Acts::KalmanFitterOptions<MeasurementCalibrator, Acts::VoidOutlierFinder, Acts::VoidReverseFilteringLogic>
      kfOptions(gctx, mfContext, calibContext, MeasurementCalibrator(measurements),
                Acts::VoidOutlierFinder(), Acts::VoidReverseFilteringLogic(), Acts::LoggerWrapper{*m_logger},
                Acts::PropagatorPlainOptions(), &(*pSurface));
  kfOptions.multipleScattering = false;
  kfOptions.energyLoss = false;

  ATH_MSG_DEBUG("Invoke fitter");
  auto result = (*m_fit)(sourceLinks, trackParameters, kfOptions);

  if (result.ok()) {
    const auto& fitOutput = result.value();
    if (fitOutput.fittedParameters) {
      const auto& params = fitOutput.fittedParameters.value();
      ATH_MSG_DEBUG("ReFitted parameters");
      ATH_MSG_DEBUG("  params:   " << params.parameters().transpose());
      ATH_MSG_DEBUG("  position: " << params.position(gctx).transpose());
      ATH_MSG_DEBUG("  momentum: " << params.momentum().transpose());
      ATH_MSG_DEBUG("  charge:   " << params.charge());
      using IndexedParams = std::unordered_map<size_t, Acts::BoundTrackParameters>;
      IndexedParams indexedParams;
      indexedParams.emplace(fitOutput.lastMeasurementIndex, std::move(params));
      std::vector<size_t> trackTips;
      trackTips.reserve(1);
      trackTips.emplace_back(fitOutput.lastMeasurementIndex);
      // trajectories.emplace_back(std::move(fitOutput.fittedStates),
      //                           std::move(trackTips),
      //                           std::move(indexedParams));
      myTrajectories.emplace_back(std::move(fitOutput.fittedStates),
                                  std::move(trackTips),
                                  std::move(indexedParams));
    } else {
      ATH_MSG_DEBUG("No fitted parameters for track");
    }
    newTrack = makeTrack(gctx, result);
  }

  if (m_statesWriter && !m_noDiagnostics) {
    StatusCode statusStatesWriterTool = m_trajectoryStatesWriterTool->write(gctx, myTrajectories, isMC);
  }
  if (m_summaryWriter && !m_noDiagnostics) {
    StatusCode statusSummaryWriterTool = m_trajectorySummaryWriterTool->write(gctx, myTrajectories, isMC);
  }

  return newTrack;
}


namespace {

using Updater = Acts::GainMatrixUpdater;
using Smoother = Acts::GainMatrixSmoother;
using Stepper = Acts::EigenStepper<>;
using Propagator = Acts::Propagator<Stepper, Acts::Navigator>;
using Fitter = Acts::KalmanFitter<Propagator, Updater, Smoother>;

struct TrackFitterFunctionImpl
    : public KalmanFitterTool::TrackFitterFunction {
  Fitter trackFitter;

  TrackFitterFunctionImpl(Fitter &&f) : trackFitter(std::move(f)) {}

  KalmanFitterTool::TrackFitterResult operator()(
      const std::vector<IndexSourceLink> &sourceLinks,
      const KalmanFitterTool::TrackParameters &initialParameters,
      const KalmanFitterTool::TrackFitterOptions &options)
  const override {
    return trackFitter.fit(sourceLinks, initialParameters, options);
  };
};

}  // namespace


std::shared_ptr<KalmanFitterTool::TrackFitterFunction>
KalmanFitterTool::makeTrackFitterFunction(
    std::shared_ptr<const Acts::TrackingGeometry> trackingGeometry) {
  auto magneticField = std::make_shared<FASERMagneticFieldWrapper>();
  auto stepper = Stepper(std::move(magneticField));
  Acts::Navigator::Config cfg{trackingGeometry};
  cfg.resolvePassive = false;
  cfg.resolveMaterial = true;
  cfg.resolveSensitive = true;
  Acts::Navigator navigator(cfg);
  Propagator propagator(std::move(stepper), std::move(navigator));
  Fitter trackFitter(std::move(propagator));
  return std::make_shared<TrackFitterFunctionImpl>(std::move(trackFitter));
}


Acts::MagneticFieldContext KalmanFitterTool::getMagneticFieldContext(const EventContext& ctx) const {
  SG::ReadCondHandle<FaserFieldCacheCondObj> readHandle{m_fieldCondObjInputKey, ctx};
  if (!readHandle.isValid()) {
    std::stringstream msg;
    msg << "Failed to retrieve magnetic field condition data " << m_fieldCondObjInputKey.key() << ".";
    throw std::runtime_error(msg.str());
  }
  const FaserFieldCacheCondObj* fieldCondObj{*readHandle};
  return Acts::MagneticFieldContext(fieldCondObj);
}


std::tuple<std::vector<IndexSourceLink>, std::vector<Measurement>>
KalmanFitterTool::getMeasurementsFromTrack(const Trk::Track &track) const {
  const int kSize = 1;
  std::array<Acts::BoundIndices, kSize> Indices = {Acts::eBoundLoc0};
  using ThisMeasurement = Acts::Measurement<IndexSourceLink, Acts::BoundIndices, kSize>;
  using IdentifierMap = std::map<Identifier, Acts::GeometryIdentifier>;

  std::shared_ptr<IdentifierMap> identifierMap = m_trackingGeometryTool->getIdentifierMap();
  std::vector<IndexSourceLink> sourceLinks;
  std::vector<Measurement> measurements;
  for (const Trk::MeasurementBase *meas : *track.measurementsOnTrack()) {
    const auto* clusterOnTrack = dynamic_cast<const Tracker::FaserSCT_ClusterOnTrack*>(meas);
    const Tracker::FaserSCT_Cluster* cluster = clusterOnTrack->prepRawData();
    if (clusterOnTrack) {
      Identifier id = clusterOnTrack->identify();
      Identifier waferId = m_idHelper->wafer_id(id);
      if (identifierMap->count(waferId) != 0) {
        Acts::GeometryIdentifier geoId = identifierMap->at(waferId);
        IndexSourceLink sourceLink(geoId, measurements.size(), cluster);
        Eigen::Matrix<double, 1, 1> pos {meas->localParameters()[Trk::locX],};
        Eigen::Matrix<double, 1, 1> cov {0.08 * 0.08 / 12};
        ThisMeasurement actsMeas(sourceLink, Indices, pos, cov);
        sourceLinks.push_back(sourceLink);
        measurements.emplace_back(std::move(actsMeas));
      }
    }
  }
  return std::make_tuple(sourceLinks, measurements);
}


Acts::BoundTrackParameters
KalmanFitterTool::getParametersFromTrack(const Trk::TrackParameters *inputParameters,
                                         const Acts::BoundVector& inputVector, double origin) const {
  using namespace Acts::UnitLiterals;
  std::shared_ptr<const Acts::Surface> pSurface = Acts::Surface::makeShared<Acts::PlaneSurface>(
      Acts::Vector3 {0, 0, origin}, Acts::Vector3{0, 0, -1});

  auto atlasParam = inputParameters->parameters();
  auto center = inputParameters->associatedSurface().center();
  // Acts::BoundVector params {center[Trk::locY], center[Trk::locX],
  //                           atlasParam[Trk::phi0], atlasParam[Trk::theta],
  //                           charge / (inputParameters->momentum().norm() * 1_MeV),
  //                           0.};
  Acts::BoundVector params;
  if (inputVector == Acts::BoundVector::Zero()) {
    params(0.0) = center[Trk::locY];
    params(1.0) = center[Trk::locX];
    params(2.0) = atlasParam[Trk::phi0];
    params(3.0) = atlasParam[Trk::theta];
    // input charge is 1 for positively charged particles and 0 for negatively charged particles!
    double charge = 2 * inputParameters->charge() - 1;
    params(4.0) = charge / (inputParameters->momentum().norm() * 1_MeV);
    params(5.0) = 0.;
  } else {
    params = inputVector;
  }
  Acts::BoundSymMatrix cov = Acts::BoundSymMatrix::Identity();
  cov.topLeftCorner(5, 5) = *inputParameters->covariance();

  for(int i=0; i < cov.rows(); i++){
    cov(i, 4) = cov(i, 4)/1_MeV;
  }
  for(int i=0; i < cov.cols(); i++){
    cov(4, i) = cov(4, i)/1_MeV;
  }
  for (int i = 0; i < 6; ++i) {
    cov(i,i) = m_seedCovarianceScale * cov(i,i);
  }

  return Acts::BoundTrackParameters(pSurface, params, inputParameters->charge(), cov);
}


std::unique_ptr<Trk::Track>
KalmanFitterTool::makeTrack(Acts::GeometryContext& geoCtx, TrackFitterResult& fitResult) const {
  using ConstTrackStateProxy =
      Acts::detail_lt::TrackStateProxy<IndexSourceLink, 6, true>;
  std::unique_ptr<Trk::Track> newtrack = nullptr;
  //Get the fit output object
  const auto& fitOutput = fitResult.value();
  if (fitOutput.fittedParameters) {
    DataVector<const Trk::TrackStateOnSurface>* finalTrajectory = new DataVector<const Trk::TrackStateOnSurface>{};
    std::vector<std::unique_ptr<const Acts::BoundTrackParameters>> actsSmoothedParam;
    // Loop over all the output state to create track state
    fitOutput.fittedStates.visitBackwards(fitOutput.lastMeasurementIndex, [&](const ConstTrackStateProxy& state) {
      auto flag = state.typeFlags();
      if (state.referenceSurface().associatedDetectorElement() != nullptr) {
        // We need to determine the type of state
        std::bitset<Trk::TrackStateOnSurface::NumberOfTrackStateOnSurfaceTypes> typePattern;
        const Trk::TrackParameters *parm;

        // State is a hole (no associated measurement), use predicted para meters
        if (flag[Acts::TrackStateFlag::HoleFlag] == true) {
          const Acts::BoundTrackParameters actsParam(state.referenceSurface().getSharedPtr(),
                                                     state.predicted(),
                                                     state.predictedCovariance());
          parm = ConvertActsTrackParameterToATLAS(actsParam, geoCtx);
          // auto boundaryCheck = m_boundaryCheckTool->boundaryCheck(*p arm);
          typePattern.set(Trk::TrackStateOnSurface::Hole);
        }
          // The state was tagged as an outlier, use filtered parameters
        else if (flag[Acts::TrackStateFlag::OutlierFlag] == true) {
          const Acts::BoundTrackParameters actsParam(state.referenceSurface().getSharedPtr(),
                                                     state.filtered(), state.filteredCovariance());
          parm = ConvertActsTrackParameterToATLAS(actsParam, geoCtx);
          typePattern.set(Trk::TrackStateOnSurface::Outlier);
        }
          // The state is a measurement state, use smoothed parameters
        else {
          const Acts::BoundTrackParameters actsParam(state.referenceSurface().getSharedPtr(),
                                                     state.smoothed(), state.smoothedCovariance());
          actsSmoothedParam.push_back(std::make_unique<const Acts::BoundTrackParameters>(Acts::BoundTrackParameters(actsParam)));
          //  const auto& psurface=actsParam.referenceSurface();
          // Acts::Vector2 local(actsParam.parameters()[Acts::eBoundLoc0], actsParam.parameters()[Acts::eBoundLoc1]);
          //  const Acts::Vector3 dir = Acts::makeDirectionUnitFromPhiTheta(actsParam.parameters()[Acts::eBoundPhi], actsParam.parameters()[Acts::eBoundTheta]);
          //  auto pos=actsParam.position(tgContext);
          parm = ConvertActsTrackParameterToATLAS(actsParam, geoCtx);
          typePattern.set(Trk::TrackStateOnSurface::Measurement);
        }
        Tracker::FaserSCT_ClusterOnTrack* measState = nullptr;
        if (state.hasUncalibrated()) {
          const Tracker::FaserSCT_Cluster* fitCluster = state.uncalibrated().hit();
          if (fitCluster->detectorElement() != nullptr) {
            measState = new Tracker::FaserSCT_ClusterOnTrack{
                fitCluster,
                Trk::LocalParameters{
                    Trk::DefinedParameter{fitCluster->localPosition()[0], Trk::loc1},
                    Trk::DefinedParameter{fitCluster->localPosition()[1], Trk::loc2}
                },
                fitCluster->localCovariance(),
                m_idHelper->wafer_hash(fitCluster->detectorElement()->identify())
            };
          }
        }
        double nDoF = state.calibratedSize();
        const Trk::FitQualityOnSurface *quality = new Trk::FitQualityOnSurface(state.chi2(), nDoF);
        const Trk::TrackStateOnSurface *perState = new Trk::TrackStateOnSurface(measState, parm, quality, nullptr, typePattern);
        // If a state was successfully created add it to the trajectory
        if (perState) {
          finalTrajectory->insert(finalTrajectory->begin(), perState);
        }
      }
      return;
    });

    // Create the track using the states
    const Trk::TrackInfo newInfo(Trk::TrackInfo::TrackFitter::KalmanFitter, Trk::ParticleHypothesis::muon);
    // Trk::FitQuality* q = nullptr;
    // newInfo.setTrackFitter(Trk::TrackInfo::TrackFitter::KalmanFitter     ); //Mark the fitter as KalmanFitter
    newtrack = std::make_unique<Trk::Track>(newInfo, std::move(*finalTrajectory), nullptr);
  }
  return newtrack;
}


const Trk::TrackParameters*
KalmanFitterTool::ConvertActsTrackParameterToATLAS(const Acts::BoundTrackParameters &actsParameter, const Acts::GeometryContext& gctx) const {
  using namespace Acts::UnitLiterals;
  std::optional<AmgSymMatrix(5)> cov = std::nullopt;
  if (actsParameter.covariance()){
    AmgSymMatrix(5) newcov(actsParameter.covariance()->topLeftCorner(5, 5));
    // Convert the covariance matrix to GeV
    for(int i=0; i < newcov.rows(); i++){
      newcov(i, 4) = newcov(i, 4)*1_MeV;
    }
    for(int i=0; i < newcov.cols(); i++){
      newcov(4, i) = newcov(4, i)*1_MeV;
    }
    cov =  std::optional<AmgSymMatrix(5)>(newcov);
  }
  const Amg::Vector3D& pos=actsParameter.position(gctx);
  double tphi=actsParameter.get<Acts::eBoundPhi>();
  double ttheta=actsParameter.get<Acts::eBoundTheta>();
  double tqOverP=actsParameter.get<Acts::eBoundQOverP>()*1_MeV;
  double p = std::abs(1. / tqOverP);
  Amg::Vector3D tmom(p * std::cos(tphi) * std::sin(ttheta), p * std::sin(tphi) * std::sin(ttheta), p * std::cos(ttheta));
  const Trk::CurvilinearParameters * curv = new Trk::CurvilinearParameters(pos,tmom,tqOverP>0, cov);
  return curv;
}

