/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS and FASER collaborations
*/

#include "FaserSCT_NoisyStripTool.h"


FaserSCT_NoisyStripTool::FaserSCT_NoisyStripTool (const std::string& type,
                                                  const std::string& name, const IInterface* parent) :
    base_class(type, name, parent) {}


StatusCode FaserSCT_NoisyStripTool::initialize() {
  return StatusCode::SUCCESS;
}


StatusCode FaserSCT_NoisyStripTool::finalize() {
  return StatusCode::SUCCESS;
}


std::map<std::pair<int, int>, double>
FaserSCT_NoisyStripTool::getNoisyStrips(const EventContext& /*ctx*/) const {
  // TODO fix hard-coded definition of noisy strip and read from NoisyPixels.xml database
  std::map<std::pair<int, int>, double> noisyStrips {};
  // noisyStrips.insert({std::make_pair(10, 150), 0.25279282895176935});
  // noisyStrips.insert({std::make_pair(20, 155), 0.9950721341449819});
  // noisyStrips.insert({std::make_pair(38, 41), 1.0});
  // noisyStrips.insert({std::make_pair(48, 643), 0.6209110977322898});
  // noisyStrips.insert({std::make_pair(49, 69), 1.0});
  // noisyStrips.insert({std::make_pair(49, 508), 0.1250027872544429});
  // noisyStrips.insert({std::make_pair(49, 660), 1.0});
  // noisyStrips.insert({std::make_pair(61, 350), 0.3669587709322809});
  // noisyStrips.insert({std::make_pair(61, 351), 0.32956496532655477});
  // noisyStrips.insert({std::make_pair(64, 287), 1.0});
  // noisyStrips.insert({std::make_pair(65, 323), 0.5987691484380226});
  // noisyStrips.insert({std::make_pair(67, 147), 1.0});
  // noisyStrips.insert({std::make_pair(67, 207), 1.0});
  // noisyStrips.insert({std::make_pair(67, 346), 0.6463977523580173});
  // noisyStrips.insert({std::make_pair(83, 114), 0.9968113809173412});
  // noisyStrips.insert({std::make_pair(86, 544), 0.46609583695676415});
  // noisyStrips.insert({std::make_pair(96, 79), 1.0});
  // noisyStrips.insert({std::make_pair(96, 183), 1.0});
  // noisyStrips.insert({std::make_pair(97, 550), 1.0});
  // noisyStrips.insert({std::make_pair(100, 215), 1.0});
  // noisyStrips.insert({std::make_pair(100, 610), 1.0});
  // noisyStrips.insert({std::make_pair(130, 722), 1.0});
  // noisyStrips.insert({std::make_pair(132, 297), 0.8765803732691151});
  // noisyStrips.insert({std::make_pair(144, 597), 1.0});
  // noisyStrips.insert({std::make_pair(144, 665), 1.0});
  // noisyStrips.insert({std::make_pair(145, 9), 1.0});
  // noisyStrips.insert({std::make_pair(145, 492), 1.0});
  // noisyStrips.insert({std::make_pair(145, 566), 1.0});
  // noisyStrips.insert({std::make_pair(146, 267), 1.0});
  // noisyStrips.insert({std::make_pair(147, 393), 0.4758623765246282});
  // noisyStrips.insert({std::make_pair(147, 394), 0.5172252324570206});
  // noisyStrips.insert({std::make_pair(147, 395), 0.5058532343300556});
  // noisyStrips.insert({std::make_pair(147, 396), 0.5272816464869445});
  // noisyStrips.insert({std::make_pair(147, 397), 0.4782036702566504});
  // noisyStrips.insert({std::make_pair(147, 398), 0.5092202376970589});
  // noisyStrips.insert({std::make_pair(147, 399), 0.4811247129127924});
  // noisyStrips.insert({std::make_pair(147, 600), 1.0});
  // noisyStrips.insert({std::make_pair(151, 417), 0.6434767097018753});
  // noisyStrips.insert({std::make_pair(152, 698), 0.2651013445715433});
  // noisyStrips.insert({std::make_pair(152, 699), 0.3080250629919504});
  // noisyStrips.insert({std::make_pair(153, 396), 0.4862532610876982});
  // noisyStrips.insert({std::make_pair(153, 397), 0.44509108747519344});
  // noisyStrips.insert({std::make_pair(154, 620), 0.4075634936562089});
  // noisyStrips.insert({std::make_pair(154, 621), 0.5001449372310299});
  // noisyStrips.insert({std::make_pair(155, 146), 0.4566637679220461});
  // noisyStrips.insert({std::make_pair(155, 147), 0.4941021695988584});
  // noisyStrips.insert({std::make_pair(158, 737), 0.5486208665016612});
  // noisyStrips.insert({std::make_pair(158, 738), 0.5591901353490758});
  // noisyStrips.insert({std::make_pair(158, 739), 0.5590786451713604});
  // noisyStrips.insert({std::make_pair(160, 7), 1.0});
  // noisyStrips.insert({std::make_pair(161, 763), 1.0});
  // noisyStrips.insert({std::make_pair(164, 613), 1.0});
  // noisyStrips.insert({std::make_pair(167, 175), 1.0});
  // noisyStrips.insert({std::make_pair(170, 90), 0.48484848484848486});
  // noisyStrips.insert({std::make_pair(170, 91), 0.4570874305973644});
  // noisyStrips.insert({std::make_pair(173, 18), 1.0});
  // noisyStrips.insert({std::make_pair(173, 484), 1.0});
  // noisyStrips.insert({std::make_pair(174, 230), 1.0});
  // noisyStrips.insert({std::make_pair(174, 530), 1.0});
  // noisyStrips.insert({std::make_pair(175, 683), 1.0});
  // noisyStrips.insert({std::make_pair(176, 418), 1.0});
  // noisyStrips.insert({std::make_pair(177, 149), 1.0});
  // noisyStrips.insert({std::make_pair(177, 345), 1.0});
  // noisyStrips.insert({std::make_pair(178, 214), 1.0});
  // noisyStrips.insert({std::make_pair(178, 508), 0.5192097576203537});
  // noisyStrips.insert({std::make_pair(178, 673), 0.6028496889424042});
  // noisyStrips.insert({std::make_pair(179, 651), 0.999977701964457});
  // noisyStrips.insert({std::make_pair(182, 525), 0.5044707561263853});
  // noisyStrips.insert({std::make_pair(182, 526), 0.5083506143108792});
  // noisyStrips.insert({std::make_pair(185, 493), 0.42738644725399694});
  // noisyStrips.insert({std::make_pair(185, 494), 0.43757664949717934});
  // noisyStrips.insert({std::make_pair(187, 427), 0.9203737150757019});
  // noisyStrips.insert({std::make_pair(188, 696), 0.6201752625593685});
  // noisyStrips.insert({std::make_pair(188, 752), 1.0});
  // noisyStrips.insert({std::make_pair(189, 249), 0.1250027872544429});
  // noisyStrips.insert({std::make_pair(189, 338), 0.25925925925925924});
  // noisyStrips.insert({std::make_pair(191, 170), 1.0});
  // noisyStrips.insert({std::make_pair(191, 406), 1.0});
  return noisyStrips;
}


std::map<std::pair<int, int>, double>
FaserSCT_NoisyStripTool::getNoisyStrips() const {
  const EventContext& ctx{Gaudi::Hive::currentContext()};
  return getNoisyStrips(ctx);
}
